//! Bindings to [splatoon3.ink](https://splatoon3.ink/)'s API.
//!
//! This project is not official or associated with splatoon3.ink or Nintendo.

use serde::de::DeserializeOwned;

type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    ReqwestError(#[from] reqwest::Error),
}

pub struct Splat3Ink {
    client: reqwest::Client,
    cache_schedules: Option<Schedules>,
}

impl Splat3Ink {
    pub fn new() -> Result<Self> {
        Self::with_user_agent("splat3ink rust api (https://codeberg.org/Cyborus/splat3ink)")
    }

    pub fn with_user_agent(agent: &str) -> Result<Self> {
        let client = reqwest::Client::builder().user_agent(agent).build()?;
        Ok(Self {
            client,
            cache_schedules: None,
        })
    }

    async fn get<T: DeserializeOwned>(&self, path: &str) -> Result<T> {
        Ok(self.client.get(path).send().await?.json().await?)
    }

    pub async fn schedules(&mut self) -> Result<Schedules> {
        match &self.cache_schedules {
            Some(schedule)
                if schedule.data.regular_schedules.nodes[0].end_time
                    > time::OffsetDateTime::now_utc() =>
            {
                Ok(schedule.clone())
            }
            _ => {
                let schedule = self
                    .get::<Schedules>("https://splatoon3.ink/data/schedules.json")
                    .await?;
                self.cache_schedules = Some(schedule.clone());
                Ok(schedule)
            }
        }
    }
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct Schedules {
    pub data: SchedulesData,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct SchedulesData {
    pub regular_schedules: RegularSchedules,
    pub bankara_schedules: BankaraSchedules,
    pub x_schedules: XSchedules,
    pub coop_grouping_schedule: CoopGroupingSchedule,
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct RegularSchedules {
    pub nodes: Vec<RegularSchedule>,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct RegularSchedule {
    #[serde(with = "time::serde::rfc3339")]
    pub start_time: time::OffsetDateTime,
    #[serde(with = "time::serde::rfc3339")]
    pub end_time: time::OffsetDateTime,
    pub regular_match_setting: RegularMatchSetting,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct RegularMatchSetting {
    pub vs_stages: [VsStage; 2],
    pub vs_rule: VsRule,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct VsStage {
    pub vs_stage_id: u32,
    pub name: String,
    pub image: Image,
    pub id: String,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Image {
    pub url: url::Url,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct VsRule {
    pub name: String,
    pub rule: GameMode,
    pub id: String,
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct BankaraSchedules {
    pub nodes: Vec<BankaraSchedule>,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct BankaraSchedule {
    #[serde(with = "time::serde::rfc3339")]
    pub start_time: time::OffsetDateTime,
    #[serde(with = "time::serde::rfc3339")]
    pub end_time: time::OffsetDateTime,
    pub bankara_match_settings: [BankaraMatchSetting; 2],
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct BankaraMatchSetting {
    pub vs_stages: [VsStage; 2],
    pub vs_rule: VsRule,
    pub bankara_mode: String,
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct XSchedules {
    pub nodes: Vec<XSchedule>,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct XSchedule {
    #[serde(with = "time::serde::rfc3339")]
    pub start_time: time::OffsetDateTime,
    #[serde(with = "time::serde::rfc3339")]
    pub end_time: time::OffsetDateTime,
    pub x_match_setting: XMatchSetting,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct XMatchSetting {
    pub vs_stages: [VsStage; 2],
    pub vs_rule: VsRule,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct CoopGroupingSchedule {
    pub regular_schedules: CoopRegularSchedules,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct CoopRegularSchedules {
    pub nodes: Vec<CoopRegularSchedule>,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct CoopRegularSchedule {
    #[serde(with = "time::serde::rfc3339")]
    pub start_time: time::OffsetDateTime,
    #[serde(with = "time::serde::rfc3339")]
    pub end_time: time::OffsetDateTime,
    pub setting: CoopSetting,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct CoopSetting {
    pub boss: Boss,
    pub coop_stage: CoopStage,
    pub weapons: [Weapon; 4],
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Boss {
    pub name: String,
    pub id: String,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct CoopStage {
    pub name: String,
    pub thumbnail_image: Image,
    pub image: Image,
    pub id: String,
}

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Weapon {
    pub name: String,
    pub image: Image,
}

#[derive(serde::Deserialize, Debug, Clone)]
pub enum GameMode {
    #[serde(rename = "TURF_WAR")]
    TurfWar,
    #[serde(rename = "AREA")]
    SplatZones,
    #[serde(rename = "LOFT")]
    TowerControl,
    #[serde(rename = "GOAL")]
    Rainmaker,
    #[serde(rename = "CLAM")]
    ClamBlitz,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test() {
        let client = Splat3Ink::with_user_agent(
            "splat3ink rust api [TESTING] (https://codeberg.org/Cyborus/splat3ink)",
        )
        .unwrap();
        dbg!(client.schedules().await.unwrap());
        panic!();
    }
}
